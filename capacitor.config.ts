import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'marvel-api',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
