import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MarvelService } from 'src/app/services/marvel.service';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.page.html',
  styleUrls: ['./characters.page.scss'],
})
export class CharactersPage implements OnInit {
  private characters: Observable<any>;
  constructor(private marvelService: MarvelService) { 
    this.characters = this.marvelService.getCharacters();
    this.characters.subscribe(res => console.log(res));
  }

  ngOnInit() {
  }

}