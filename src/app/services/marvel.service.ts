import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class MarvelService {

  private apiKey: string = "b3b8cc7702b571d4e1357bfc96c707a2";
  private hash: string = "e4836c5b7f70e0c0cd08b35ba7d4448a";
  private url: string = "http://gateway.marvel.com/v1/public/";
  private queryStr: string = '?ts=100&apikey='+this.apiKey+'&hash='+this.hash;

  constructor(private http: HttpClient) { }

  getCharacters(){
    return this.http.get<any>(this.url+'characters'+this.queryStr);
  }
  getComics(){
    return this.http.get<any>(this.url+'comics'+this.queryStr);
  }
}